---
layout: handbook-page-toc
title: "Channels"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Channels OKR

[OKRs](/company/okrs/)

### Channels Value 

The Channel is a critical part of our strategy moving forward as it will help us:
1. Drive growth ARR through services capacity and capability to drive customer adoption and usage of the GitLab platform.
1. Drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

![GitLab Channel Value](/handbook/sales/channel/images/channel_handbook1.png)

The purpose of this page is to provide the  GitLab Sales team insights on the GitLab channel partner community and how to best work with our partners.

Additional related channel resources:



*   [Channel Operations Page](/handbook/sales/field-operations/channel-operations/)  - Learn how to manage partner deals in Salesforce and other operational tips when working with partners.
*   [Channel Partner Program page]( /handbook/resellers/) - This page provides an overview of the GitLab channel partner program, the expectations of partners, benefits for partner and how partners can work with GitLab. 


## **Channels Partner Types**


### **Resellers**

Primary monetization is through reselling GitLab licenses and services. Resellers can be a service partner too (often known as a Solution Provider).



1. VAR/VAD (Value Added Reseller or Distributor): Channel services including resale, implementation, contracting, support, financing etc.
2. DMR (Direct Market Reseller): Primary business is resale of the software, often does not implement. Value are the contracts that these partners have in place with customers.


### **Services partners**

Primary monetization is through the sale of services. This can be a one-time implementation, ongoing support or advisory, managed services, or outsourcing. Services partners will resell GitLab services, deliver services on behalf of GitLab or deliver GitLab certified services. Services partners can be a reseller partner too (often known as a Solution Provider).



1. Global Systems Integrators have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
2. Regional Systems Integrators are large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
3. Boutique Systems Integrators are very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - CloudReach, Flux7
4. Managed Service Providers provide ongoing support for solutions/applications. Examples: Rackspace


### **Alliances**

[Alliances](/handbook/alliances/)

**How can GitLab sellers benefit from our channel partners?**

GitLab channel partners have established sales forces that help us multiply our sales coverage.  Through that coverage and deep customer relationships, partners can identify new customer opportunities and new sales opportunities with existing customers, resulting in ARR growth.  Through their service offerings and vendor partnerships, they can deliver more complete solutions than GitLab can alone, and drive customer adoption of GitLab.  As a result, the GitLab channel will:



1. Improve customer reach and experience
    1. Resellers/SI’s that know & can make intros to  your customers
    2. Ease of order processing
2. Channel leverage
    3. Resellers/SI’s that bring us into new opportunities & grow existing
    4. Decreased Customer Acquisition Cost 
    5. Improved Net & Gross Retention
3. Services Capacity to Adopt and Expand GitLab deployments
    6. SI’s that help our customers deliver more value 
    7. Stage Monthly Active Users Acceleration
4. Market Position
    8. Acquire partners from the Atlassian/Cloud Bees channel 
    9. Monetize the GitHub channel 
    10. Align to the AWS channel

**Partners can help you scale and working with them is comp neutral**

**[Channel Neutral Compensation](/handbook/sales/commissions/#channel-neutral-compensation)**

FY21 commissions will be channel neutral for all deals through partners (including Distributors, if applicable), which means standard partner discounts are credited back to the salesperson and must follow [order processing procedures](/handbook/business-ops/order-processing/) to correctly flag partner attribution. Total IACV on the deal after all discounts will count towards quota credit, but the channel neutral amount does not qualify for quota credit and only pays out for compensation at BCR. See Channel Neutral section referenced in the [FY21 Commission Plan presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g7da31a6494_6_0)

Partner Services also qualify for [FY21 Professional Services Spiff (v2)](/handbook/sales/commissions/#fy21-professional-services-spiff-v2).  So as partners help customers adopt and expand their GitLab deployments, they can earn a Services Attach rebate and the GitLab sales team can earn the Professional Services Spiff.

**Maximizing value by working with Channel Partners**

Developing successful relationships with channel partners in your region requires some effort, but has a great payback.  Following are a few key tips that will help you develop relationships with GitLab channel partners and greatly increase your sales effectiveness.



*   **Honor Partner ←→ Customer Relationships**
    *   **Respect Deal Reg’s from Partners:  <span style="text-decoration:underline;">No</span>** direct sales of opportunities with approved Partner Deal  Reg without prior approval of VP, Global Channel Sales 
    *   **Recognize Incumbent Partners***: Do not bring another Partner into your customer convos
*   **For new Deal Reg’s in your pipeline**
    *   Be prepared to construct a simple joint pursuit plan in SFDC (CAMs are here to support same)
    *   Strive to team with the trained Partner people
*   **The program determines the partner discount**
    *   Our agreement with partners is that by initiating, assisting or fulfilling sales opportunities, or selling services, partners will earn set amount based on their partner track.
    *   These discounts are determined by the program and not GitLab Sales on individual opportunities.
*   **Respect incumbent partners**
    *   Support them in driving renewals and incremental sales
    *   Do not bring competing partners into the account
*   **Stay Focused on the Customer’s Needs & Priorities**
    *   Partners can complete a customer solution with additional products and services
*   **Give to Get (LevelUp coming)**
    *   Bring Select/Open partners into GitLab opportunities
    *   Ask them about opportunities they may have for GitLab
*   **Collaborate with Partners in your Region (LevelUp coming)**
    *   Treat them as an extension of your team
    *   Plan with key partners in your region
    *   Communicate regularly with them around sales opportunities and pipelines
    *   Help keep GitLab top of mind with them.  Educate them on GitLab.
*   **If misalignments or escalations arise during deal pursuits**
    *   Contact the CAM
    *   Chat with your peers that have channel co-selling experience
    *   Escalate to your manager 
    *   No customer collisions: Strive to stay aligned \
on customer calls - take differing POVs offline 

### GitLab Channels Program Updates - April 2020

1. Building a channel of enabled, DevOps & Digital Transformation focused resellers and services providers
1. Provide eStore access for SMB & Midmarket channel partners
1. Net neutral to GitLab seller compensation
1. Incentives to identify net new customers & opportunities in existing customers
1. Incentives to attach product, operational & strategic services
1. Referral fees for non reselling services partners
1. MDF available for Demand Generation activities and events
1. Sales & SE Enablement available on demand; certifications - H2’FY21
1. Services Certifications - H2’FY21
1. Renewals incumbent protection: If a partner sells a deal and is in good standing (actively supporting the customer, etc) that partner receives first right of refusal for renewal; unless otherwise stated by the customer

#  Channel Operations

##  Channel Neutral

To incentivize working with our Channel partners, 'Channel Neutral' means that we will not reduce $ value to individual sellers even if the Channel business reduces total iACV to GitLab (via disocunts or rebates).
More information can be found on the [compensation page](/handbook/sales/commissions/#channel-neutral-compensation).

##  Definitions

**Program Compliance**
- For partners to transact, they must join the GitLab Partner Program via the partner portal.
- Non Contracted partners may transact on a one-off basis, only with approval of channel leadership.

**PIO - Partner Initiated Opportunity**
- Any opportunity that was brought to GitLab via a Deal Registration.
- The opportunity must be new to our sales team, and can be for a new or existing customer.
- This is an upfront discount and is dependent on the partners track within the GitLab Partner Program.

**Channel Assist Opportunity**
- Any opportunity where the partner assists our sales team to close the deal.
- This may be a customer demo, an executive introduction meeting, delivery of services, etc.
- This would be a Deal Registration, but for a GitLab sourced opportunity, so it does not qualify for PIO.
- The determination of Channel Assist is at the sales rep determination and tracked via SFDC opportunities.

**Channel Fulfill Opportunity**
- Any opportunity that was fulfilled by a partner but closed independently via the GitLab sales team.
- The partner has only processed the order and didn’t provide any meaningful support to close the deal.

**Services Attach**
- Any partner delivered services that are provided to the end user in support of a GitLab deployment.
- This will result in a 2.5% upfront discount from the product.
- This is stackable for up to three (3) independent services provided by the partner to a single end user.
- This will be administered as an upfront discount from the GitLab license price on the initial sale.
- The partner may sell their own services, after the initial sale, for a rebate of 2.5% of the Net license price for a period of up to nine (9) months.
- The maximum is 7.5% discount or rebate with any combination of upfront or post sales of partner branded services.
- Services may be delivered by a non-Authorized service partner, which will be paid as a referral fee.
- Rebates and referral fees may require CRO approval.

**Services Resale**
- Any partner that resales services that will be delivered by the GitLab Professional Services team.

**Channel Neutral Comp**
- For FY21 the sales team will not absorb any channel partners standard contractual discounts.
- They will be compensated at the non-channel net value of the deal.

**Incumbency Renewal Policy**
- If a partner transacts IACV, when renewal time comes, the partner will own that renewal unless formally communicated by the customer.
- The partner will be required to be program compliant (ie in good credit standing, have provided quarterly updates on customer, review within 30 days of renewal, etc).

## Frequently Asked Questions

**Where can I find the Standard Channel Discounts for my Partners?**
- Follow [SFDC Discounting table](https://gitlab.my.salesforce.com/0694M000008xAk4)

**Where can I find more information about our current Partners processes?**
- The current Resellers Handbook can be found [Here](/handbook/resellers/)

**Whats the current Deal Registration Process?**
- The current process can be found [Here](/handbook/resellers/#deal-registration)

**How do I track the opportunity of a partner deal?**
Please fill in at least one of the following Opportunity drop downs to identify the deal as a Channel opportunity.
- **If the partner registered the deal** the `Deal Registrar`field will need to be populated in the SFDC opportunity with the appropriate Partner Account.
- **If the Partner Assisted you** (see above for definition) please populate the `Partner - Assisted` field in the SFDC opportunity with the appropriate partner account.
- **If GitLab sourced and sold the deal directly, and the partner is simply fulfilling the deal**, please populate the `Partner - Fulfillment` field in the SFDC opportunity.

**How do I check what I'm being paid on?**

**What are some examples of Channel Neutral math?**

| **Deal Calculation** | **Direct Deal** | **Channel 1 (Neutral)** | **Channel 2 (Add’l Disc)** | **Channel 3 (Split Disc)** |
|----- | ----- | ------ | ------ | ------|
| List Price | $100,000 |  $100,000 | $100,000  | $100,000 |
| Channel Discount | 0% | 20% | 20% | 20% |
| Additional Rep Discount | 5% | 0% | +5% | +2.5% |
| **Total Discount** | **5%** | **20%** | **25%** | **22.5%** |
| IACV (Quota Relief) | $95,000 |  $80,000 | $75,000  | $77,500 |
| Channel Neutral (No Quota Relief @ BCR)  | +$0 |  +$20,000 | +$20,000  | +$20,000 |
| **Commissionable Amount** | **$95,000** |  **$100,000** | **$95,000**  | **$97,500** |
| Base Commission Rate (BCR) | % 8 |  % 8 | % 8  | % 8 |
| **Commission Payout** | **$7,600** | **$8,000** | **$7,600**  | **$7,800** |


**How do I get Channel deals/discount approvals?**
Follow [standard approval process](/handbook/business-ops/order-processing/#step-5---submitting-a-quote-for-discount-and-terms-approval) in SFDC 
    
**How does the Amazon Process work?**
Follow [Amazon Web Services (AWS) Private Offer Transactions](/handbook/business-ops/order-processing/#amazon-web-services-aws-private-offer-transactions) handbook

**I need HELP!  How to do I reach out to the experts?**

The quickest way to get help is by using the following Slack channels:
- #channel-sales
- #channel-ops
- #alliances

## Channels groups, projects, and labels

**Groups**
Use the GitLab.com group for epics that may include issues within and outside the Channels Team group. 
- https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel

- Guidelines for  Partner Folders:  
  - The partners Group is further divided into regional sub-groups
  - Within each region sub-group Partners will get their own group
  - Partner groups contain a collaboration project and internal project
  - Partner employees should be explicitly invited to collaboration group
  - Internal group should not be visible to non-GitLab employees and may contain licensing details or other sensitive information
  - Additional projects may be created within the partner subgroup to contain code bases for prototypes or PoV
  - Please avoid creating additional subgroups within partner groups 

**Projects**
- Create issues under the “Channels” project

**Epics**
- From FY21 Q3 - OKRs will be formed as EPICs and issues related to that Epic should be associated 
- Broad initiatives will have EPICs with several issues to support that project 

**Labels**
  - **Team labels**
      - Channel- issue initially created, used in templates, the starting point for any label that involves Channels
      - Channel Ops - label for issues that directly impact the Channel Ops & team.  DRI will be defined in the intro of the issue
      - Channel Program  - label for issues that directly impact the Dir of Channel Programs  & team. DRI will be defined in the intro of the issue
      - Channel Services- label for issues that directly impact the Channel Services Manager. DRI will be defined in the intro of the issue
      - Channel Marketing- label for issues that directly impact the Channel Marketing team. DRI will be defined in the intro of the issue
      - Channel Distribution- label for issues that directly impact the Distribution leader. DRI will be defined in the intro of the issue
      - Channel GSI - label for issues that are owned by the Dir of GSI. DRI will be defined in the intro of the issue
      - Internal Channel Enablement- label for issues that are focused on Internal Channel Enablement issues. DRI will be defined in the intro of the issue
      - Channel Handbook Needs- label for issues that are about pending or planned Channel Handbook changes. DRI will be defined in the intro of the issue
      - QBR - Requests from Sales QBRs

   - **Priority Weighting (using Eisenhower matrix and weighted tabs in Gitlab)** 
      - WEIGHT 1 ~ Channel Priority:1 - Home runs (high value to GitLab and high likelihood of success that align to Sales & Channel OKRs) and committed to completion within stated milestones. This category will be limited because not everything can be a priority.  These are both URGENT & IMPORTANT 
      - WEIGHT 2 ~ Channel Priority:2 - Big Bets (high value to GitLab, lower time urgency, longer dependencies or lower likelihood of success) within stated milestones.  These are not urgent but IMPORTANT to our success
      - WEIGHT 3 - Channel Priority:3 - Small wins within stated milestones.  These are URGENT but not strategically important.  Delegate or push out 
      - WEIGHT 4 - Channel Priority:4 - Small wins that are important but high value.  Should be slotted in where backlog allows 
      - WEIGHT 5 - Channel Priority:Backlog - Things in the queue not currently being worked LABEL 

**DRI** 
To be stated in intro of issue and assigned to that person.  There maybe 1 or more assignee but the DRI should be stated intro of issues

**Milestones**
- Milestones are within 2 week windows 
- Channel - FY2xQxM(month#)a (1st-15th) or b(16th-30th),
- Like this: Channel - **FY21Q2 3a**

**Due Dates**
What is the expected due date of completion or NBA (next best action - next key iteration and should be mentioned in the issue)? 




