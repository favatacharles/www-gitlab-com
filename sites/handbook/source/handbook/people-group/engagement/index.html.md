---
layout: markdown_page
title: "Engagement"
---
### Engagement

Team member engagement is important to the overall continued success of GitLab.  Engagement strategies have been shown to reduce attrition, improve productivity and efficiency, enhances the company values and enhanced job satisfaction.  One of the ways that GitLab tracks team member engagement is through the bi-annual Team Member Engagement Survey administered by CultureAmp.  Engagement surveys are an important tool that gives team members an opportunity to provide feedback.  It also allows GitLab leadership to gain insight into what is important to team members.  

## October 2019 Engagement Survey results 


87% of the GitLab Team participated in the Engagement Survey in October 2019.  
GitLab had an Overall Favorable score of 88% - up slightly from 83% in 2018.
[Overall Engagement Survey results slide](https://docs.google.com/presentation/d/19ZkIizrtEMmmOqMkIPTU2VxU01mKvArYoVPMmdXXdFs/edit#slide=id.g4792064fe5_0_4).

The E Group has chosen to focus on the following 3 areas over the next 8 weeks (the results were releases on November 12, 2019):
* Taking action from the survey
* Team member development 
* Total Compensation

The survey consisted of 46 questions divided into the following sections:

* GitLab Overall
* Company Confidence
* Our Leaders
* Your Manager
* Teamwork
* Your Role
* Culture
* Growth & Development
* Action
* Comments

## Benchmark and reporting

The 2019 survey will be benchmarked against New Tech Mid Size Companies (500+ team members).  The survey is completely anonymous! There are no names collected with any responses or comments.  

# Results

* The benchmark results from all companies who take the survey will be shared with leaders who have 5 or more direct reports who complete the survey. 
* The E-team will identify one key action item to work during the 6 months between surveys. 
* The GitLab survey results will be shared with everyone in the company
* Functional results engineering, sales, etc will be made available to the entire company to review.

For those that participate in future survey's, please note, once you hit submit you are unable to go in and edit your answers or add additional comments. It will show that you have completed the survey. Please use the "save and exit" option until you are ready to submit your final answers.


## 2018 GitLab overall results

In October 2018 GitLab launched it's annual enagement survey via CultureAmp.  The survey was benchmarked against 2018 New Tech - Mid Size Companies (200-500 team members).   


* 94% participation rate
* Overall engagement scores - 83% favorable, 12% neutral and 5% unfavorable

# Highest 3 Scores

* I am proud to work for GitLab - 95% favorable response
* I know how my work contribute to the goals of GitLab - 94% favorable response
* GitLab is in a position to really succeed over the next three years - 93% favorable response

# Highest 3 Scores vs Benchmark

* The leaders at GitLab have communicated a vision that motivates me - 88% favorable response and 22% above industry benchmark
* GitLab effectively directs resources (funding, people and efforts) towards company goals - 74% favorable response and 20% above industry benchmark
* At GitLab there is open and honest two-way communication - 84% favorable response and 20% above benchmark

# Lowest 3 Scores

* I have seen positive changes taking place based on recent employee survey results - 32% favorable response
* My manager or someone else has communicated clear actions based on recent team member survey results - 33% favorable response
* I believe my total compensation (base salary+any bonus+benefits+equity) is fair relative to similar roles at other companies - 43% favorable response

# Lowest 3 Scores vs Benchmark

* I have seen positive changes taking place based on recent employee survey results - 32% favorable response and 12% below industry benchmark
* My manager or someone else has communicated clear actions based on recent team member survey results - 33% favorable response and 11% below industry benchmark
* I believe my total compensation (base salary+any bonus+benefits+equity) is fair relative to similar roles at other companies - 43% favorable response and 10% below industry benchmark



The survey consists of 46 questions divided into the following sections:

* GitLab Overall
* Company Confidence
* Our Leaders
* Your Manager
* Teamwork
* Your Role
* Culture
* Growth & Development
* Action
* Comments

## Benchmark and reporting

The 2019 survey will be compared to the 2018 results.  The survey is completely anonymous! There are no names collected with any responses or comments.  

# Results

* Survey results will be shared with leaders who have 5 or more direct reports who complete the survey. 
* The E-team will identify one or two key action item to work during the 6 months between surveys. 
* The GitLab survey results will be shared with everyone in the company
* Functional results engineering, sales, etc will be made available to the entire company to review.


## Reporting

Managers with 5+ reporting team members will receive an email from Culture Amp inviting them to review their results. Division and Department leaders will also receive an email from Culture Amp inviting them to review their departments or Divisions overall results. All demographics are pulled from BambooHR and there is not a way to add custom reporting options.  

Quick tips on Culture Amp and exporting reports or utilizing the tool.  

* Different Views: When you log into the tool on the far left side you will the following different views; Insight, Participation, Questions, Custom, Heatmaps and Comments (not all may be visible based on the reporting rules).  By clicking each section allows you to view that section in more detail.
* Exporting: In the top far right hand corner you will see an export option.  You can export to PDF the questions/results that can be used to share with your team.  If you viewing from the Insight page you can export > build powerpoint and CultureAmp will create report based on your inputs.  You also have the option to choose to benchmark these against GitLab's overall results.  
* Filtering: On the far left side you will see +Add Filter.  This option will allow managers and leaders to view results based on various demographics like tenure, division and department.
* Take Action:  Managers do have the option to build an action plan and track progress via CultureAmp based on their results.  

## 2019 GitLab overall results

*  87% overall participation rate
*  Overall engagement scores - 88% favorable, 9% Neutral and 3% unfavorable

## Highest 3 scores

*  I would recommend GitLab as a great place to work - 95% favorable
*  GitLab is in a position to really succeed over the next three years - 94%
*  I am proud to work at GitLab - 94%

## Lowest 3 Scores

*  I have seen positive changes since the previous engagement survey - 29%
*  I have been provided an opportunity to see and discuss prior engagement survey results - 46%
*  I believe action will take place as a result of this survey - 64%

## 2019 E Team focus areas

*  Taking action from the engagement survey
*  Manager encouragement of team member development
*  Total compensation

## 2020 Engagement Survey Dates
* Fall survey dates - 2020-10-26

