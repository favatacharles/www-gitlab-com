---
layout: handbook-page-toc
title: "Vendor Contracts and Invoice Payment"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Procure to Pay Process

### Requirement Identification
Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money). However, any purchases requiring contracts must first be reviewed by procurement then signed off by a member of the executive team. Be sure to also check out our guide on [Signing Legal Documents](/handbook/finance/authorization-matrix/#signing-legal-documents).

#### Vendor Code of Ethics
All vendors that GitLab does business with, must legally comply with the [Supplier Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics). When having discussions with your vendor regarding the contract, make them aware of this requirement.

### Vendor and Contract Approval Workflow

For information on how to request a purchase with a third party vendor, please see the [Procure to Pay process here.](/handbook/finance/procurement/)

### Accounts Payable Performance Indicator

#### Time to Process Invoices in Tipalti <= 2 business days
The time to submit invoices to the business for approval in Tipalti. The target is <= 2 business days.


### Modern Slavery and Human Trafficking Compliance Program

GitLab condemns exploitation of humans through the illegal and degrading practices of human trafficking, slavery, servitude, forced labor, forced marriage, the sale/exploitation of chilren and adults and debt bondage (“Modern Slavery”).  To combat such illegal activities, GitLab has implemented this Modern Slavery and Human Trafficking Compliance Program.  

*Risk Areas and Markets*
While Modern Slavery can occur in any country and in any market, some regions and sectors present higher likelihood of vioaltions. Geographies with higher incidents of slavery are India, China, Pakistan, Bangladesh, Uzbekistan, Russia, Nigeria, Indonesia and Egypt. Consumer sectors such as food, tobacco and clothing are high risk sectors; but Modern Slavery can occur in many markets.*  
 (*According to Source: Statista, Walk Free Foundation, https://www.statista.com/chart/4937/modern-slavery-is-a-brutal-reality-worldwide/)  

*Actions to Address Modern Slavery Risks*
All vendors, providers and entities providing services or products to GitLab (“Vendors”) are expected to comply with [GitLab’s Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics) which specifically addresses Modern Slavery laws.  Compliance with the Partner Code of Ethics will be included in Vendor contracts and/or purchase orders going forward.  Existing Vendor contracts will be updated with the appropriate language upon renewal.

Those entities who are of higher risk or whom GitLab suspects may be in violation of Modern Slavery laws, may be required to complete an audit.  Audits may be presented in the form of a questionnaire or may be an onsite visit.  Any known or suspected violations will be raised to Legal and/or Compliance.  Failure to comply with Modern Slavery laws will result in a termination of the relationship and GitLab retains all rights in law and equity.

Vendors understand and agree that violations of Modern Slavery laws may require mandatory reporting to governing authorities. GitLab has discretion if and how to best consult with Vendors for purposes of Modern Slavery reporting. GitLab is senstive to and will take into consideration, the relationship and the risk profile of Vendor to ensure that Modern Slavery risks have been appropriately identified, assessed and addressed and that the Vendor is aware of what actions it needs to take.

*Assessment of Effectiveness*
GitLab will review its Modern Slavery and Human Trafficking Compliance Program on an annual basis or sooner if it is determined there is increased exposure or concerns with overall compliance.   The Program may be amended from time to time by GitLab, to ensure compliance with the most current Modern Slavery laws and regulations.

*Compliance Program Approval*
GitLab’s Executive Team reviewed and approves this Modern Slavery and Human Trafficking Compliance Program.
