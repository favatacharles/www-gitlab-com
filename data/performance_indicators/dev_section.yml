- name: Dev - Section MAU - Sum of Dev Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across the three stages in the Dev Section (Manage,
    Plan, and Create)
  target: 1.254M by end of Q3. This number assumes us switching to the new Create
    SMAU and including self-managed instances.
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - While Dev Section MAU will be changing significantly over Q3 with the introduction
      of Analytics views and switching to the new Create SMAU definition, the existing
      growth rate for the existing defintion seems to be acceptable. We saw 8% growth
      from June to July with healthy increases for both Create and Plan.
    - Improvements - Address the [problem areas](https://gitlab.com/groups/gitlab-org/-/epics/4104)
      that emerged out of the Category Maturity Scorecard exercise for Issue Tracking
      and continue making progress on [quality of life improvements](https://gitlab.com/groups/gitlab-org/-/epics/3216)
      for existing Plan features.
    - Improvements - Merge Request [usability improvements](https://gitlab.com/groups/gitlab-org/-/epics/3607)
  sisense_data:
    chart: 9200108
    dashboard: 704542
    embed: v2
  instrumentation:
    level: 2
    reasons:
    - Waiting on Manage SMAU (Analytics Views) [visualization](https://gitlab.com/gitlab-data/analytics/-/issues/5664)
    - Improvement - Will be switching to new Create SMAU (Git write operations) [visualization](https://gitlab.com/gitlab-data/analytics/-/issues/5629)
- name: Manage - SMAU - MAU Viewing Analytics Features
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users viewing any Analytics page (https://docs.gitlab.com/ee/user/analytics/).
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - Classified as "Problem" since we're not able to measure this, don't have a target
      defined, but are prioritizing SMAU and Analytics GMAU improvements
    - Planned improvements in 13.3 - new [throughput analytics page](https://gitlab.com/gitlab-org/gitlab/-/issues/229045)
    - Planned improvements in 13.4 - [instance-level overview](https://gitlab.com/groups/gitlab-org/-/epics/4147),
      [issues analytics improvements](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17258#note_391745446),
      and [DevOps report](https://gitlab.com/groups/gitlab-org/-/epics/4169)
  instrumentation:
    level: 2
    reasons:
    - First iteration of SMAU (analytics views) merged [into 13.3](https://gitlab.com/gitlab-org/gitlab/-/issues/224094)
      for self-managed and GitLab.com
    - Other groups beyond Manage:Analytics pending instrumentation in https://gitlab.com/groups/gitlab-org/-/epics/3690
    - After instrumentation is verified, please see [this issue](https://gitlab.com/gitlab-data/analytics/-/issues/5664)
      to track dashboard updates
- name: Manage:Access - GMAU - MAU Using Enhanced Authentication
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users logging into GitLab with LDAP, SAML, OAuth,
    or Smartcard strategies on any tier of GitLab.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - The Manage:Access group's focus is Paid GMAU so GMAU won't be an area of focus
      in the short term
  instrumentation:
    level: 1
    reasons:
    - Defined, [instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/224102)
      needed
- name: Manage:Access - Paid GMAU - MAU Using Paid SAML
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users logging into GitLab using SAML SSO for Groups.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - The ability to configure [SSO in Gitlab.com without giving users access to all
      groups](https://gitlab.com/gitlab-org/gitlab/-/issues/220203) is planned for
      13.3
    - A gallery application for [SSO for Okta](https://gitlab.com/gitlab-org/gitlab/-/issues/216173)
      is planned for 13.4
    - We are refining [SSO group sync](https://gitlab.com/gitlab-org/gitlab/-/issues/118)
      which is targeted for FY3-2020
  instrumentation:
    level: 1
    reasons:
    - Defined, [instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/231486)
      needed
- name: Manage:Compliance - Paid GMAU - MAU Viewing Compliance Features
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users viewing Audit Events, Compliance Dashboard,
    or Credential Inventory.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet. Unblocked on [GMAU instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/224109)
      below
    - View [source and destination branch data](https://gitlab.com/gitlab-org/gitlab/-/issues/216279)
      in the Compliance Dashboard expected in `13.3`
    - Export [audit events to CSV](https://gitlab.com/gitlab-org/gitlab/-/issues/1449)
      expected in `13.4`
    - Add a [Projects view](https://gitlab.com/gitlab-org/gitlab/-/issues/230826)
      to the Compliance Dashboard. Planned for `Q3 2020`
    - Improve PAT & SSH [credential management](https://gitlab.com/groups/gitlab-org/-/epics/3084)
      (epic). Ongoing with expected completion in `13.5`
    - Add [additional credentials](https://gitlab.com/groups/gitlab-org/-/epics/4110)
      (epic) to the credential inventory. Planned for `Q4 2020`
  instrumentation:
    level: 1
    reasons:
    - Defined, [instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/224109)
      expected in `13.3`
- name: Manage:Compliance - Other - Total Compliance Actions
  base_path: "/handbook/product/performance-indicators/"
  definition: A monthly count of the number of compliance actions taken.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: SaaS
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
  instrumentation:
    level: 2
    reasons:
    - We are currently collecting several of the underlying metrics from GitLab SaaS
      (see https://app.periscopedata.com/app/gitlab/663045/WIP:-Manage:-Compliance-Dashboard)
    - Additional [instrumentation](https://gitlab.com/gitlab-data/analytics/-/issues/4165#related-issues)
      needed to fill in gaps for self-managed.
    - Implementation of all 6 gaps expected to be completed by `13.6` with 3 gaps
      addressed by `13.4`
- name: Manage:Import - GMAU - MAU importing
  base_path: "/handbook/product/performance-indicators/"
  definition: A monthly count of the unique number of users performing an import.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - Improvement - To enable users to self-service GitLab migrations, the Import
      group is currently focused on [starting to iterate](https://gitlab.com/gitlab-org/gitlab/-/issues/224533)
      toward the [Complete solution](https://gitlab.com/groups/gitlab-org/-/epics/2901)
      for GitLab-to-GitLab migrations.
    - Improvement - Continue to enhance the [GitHub Importer](https://gitlab.com/groups/gitlab-org/-/epics/3051)
      with emphasis on supporting larger migrations.
  instrumentation:
    level: 1
    reasons:
    - Defined, [instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/224110)
      targeted for `13.4`
- name: Manage:Import - Other - Share of New Projects Imported
  base_path: "/handbook/product/performance-indicators/"
  definition: Share of projects created via import within 90 days of the namespace's
    creation.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - Insight - The share of imported projects has been climbing for the last 5 months
      from 11% to 17%. This coincides with the targeted effort to increase the discoverability
      of the import feature and to significantly reduce the number of known bugs.
    - Improvement - To enable users to self-service GitLab migrations, the Import
      group is currently focused on [starting to iterate](https://gitlab.com/gitlab-org/gitlab/-/issues/224533)
      toward the [Complete solution](https://gitlab.com/groups/gitlab-org/-/epics/2901)
      for GitLab-to-GitLab migrations.
    - Improvement - Continue to enhance the [GitHub Importer](https://gitlab.com/groups/gitlab-org/-/epics/3051)
      with emphasis on supporting larger migrations.
  sisense_data:
    chart: 8584850
    dashboard: 661967
    embed: v2
  instrumentation:
    level: 2
    reasons:
    - Instrumented for GitLab.com
    - Not instrumented for self-managed ([issue](https://gitlab.com/gitlab-org/gitlab/-/issues/224758)
      targeted for `13.5`)
- name: Manage:Analytics - GMAU - MAU Viewing Analytics Features
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users viewing any Analytics page (https://docs.gitlab.com/ee/user/analytics/).
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - Classified as "Problem" since we're not able to measure this yet, don't have
      a target defined, but are prioritizing Analytics GMAU improvements
    - Planned improvements in 13.3 - new [throughput analytics page](https://gitlab.com/gitlab-org/gitlab/-/issues/229045))
    - Planned improvements in 13.4 - [instance-level overview](https://gitlab.com/groups/gitlab-org/-/epics/4147),
      [issues analytics improvements](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17258#note_391745446),
      and [DevOps report](https://gitlab.com/groups/gitlab-org/-/epics/4169)
  instrumentation:
    level: 2
    reasons:
    - Merged [into 13.3](https://gitlab.com/gitlab-org/gitlab/-/issues/224094) for
      self-managed and GitLab.com
    - Dashboard [issue](https://gitlab.com/gitlab-data/analytics/-/issues/4283) pending
      development
- name: Plan - SMAU - MAU Interacting With Issues
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users interacting with an issue (open, close, reopen,
    adjust label, adjust milestone, adjust weight, comment)
  target: 196400
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - Slowed growth in the past 3mo, but inline with past trends. Up 19%
      YoY [deeper dive](https://app.periscopedata.com/app/gitlab/667053/WIP-Plan:-Project-Management-Dasboard?widget=9414271&udv=1081148).
      See further commentary under Plan:Project Management
    - Driver - Iterations & Swimlanes should drive wider adoption for Plan - especially
      Paid SMAU
  instrumentation:
    level: 3
    reasons:
    - Dashboarding - SMAU is instrumented for both .com and self-managed, but needs
      work from data team to combine data in a single dashboard and resolve lenghty
      queries. [Issue](https://gitlab.com/gitlab-data/analytics/-/issues/5682)
  sisense_data:
    chart: 9285132
    dashboard: 710545
    embed: v2
- name: Plan:Project Management - GMAU - MAU Interacting With Issues
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users interacting with Issues each month.
  target: 196400
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Actual Blended GMAU - 163,812 across SaaS and Self-Managed in July, a 1.38%
      improvement over June.
    - Insight - GMAU growth slowed considerably in June (.79%) and July (1.38%). [Further
      investigation is necessary](https://gitlab.com/gitlab-data/analytics/-/issues/4291#note_392100535),
      but there is a possible correlation between growth rates and [instances participating
      in the usage ping](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0).
      In July, 2,901 new licensed instances were brought online but total instances
      with the usage ping enabled dropped by a net of 124, resulting in 7% decline
      in the overall percentage of instances with the usage ping enabled. It is currently
      not clear how many previous instances churned or disabled their usage ping compared
      to how many of the new instances were provisioned with the usage ping disabled
      by default.
    - Improvement - Work towards addressing the [problem areas](https://gitlab.com/groups/gitlab-org/-/epics/4104)
      that emerged out of the Category Maturity Scorecard exercise for Issue Tracking.
    - Improvement - Continue making progress on [quality of life improvements](https://gitlab.com/groups/gitlab-org/-/epics/3216)
      for existing features.
  instrumentation:
    level: 2
    reasons:
    - Dashboard - Need to [collect aggregate event driven interactions](https://gitlab.com/gitlab-data/analytics/-/issues/4291).
    - Implementation - [Missing many interaction events from self-managed](https://gitlab.com/gitlab-org/gitlab/-/issues/229918)
      so the target / current MAU is likely much lower than once full implementation
      is complete.
  sisense_data:
    chart: 9422500
    dashboard: 722212
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/722212/Plan:-Project-Management-GMAU
- name: Plan:Project Management - Paid GMAU - MAU Interacting With Issues
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of paid active users interacting with Issues each month.
  target: 50400
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Actual Blended Paid GMAU - 47,440 across SaaS and Self-Managed in July, a 0.47%
      improvement over June.
    - Insight - Paid GMAU growth slowed considerably in July (.47%) compared to June
      (5.76%). SaaS fell from 2.5% (June) to 1.8% (July) while Self-Managed fell from
      8.2% (June) to -.5% (July). [Further investigation is necessary](https://gitlab.com/gitlab-data/analytics/-/issues/4291#note_392100535),
      but there is a possible correlation between growth rates and [instances participating
      in the usage ping](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0).
      In July, 2,901 new licensed instances were brought online but total instances
      with the usage ping enabled dropped by a net of 124, resulting in 7% decline
      in the overall percentage of instances with the usage ping enabled. It is currently
      not clear how many previous instances churned or disabled their usage ping compared
      to how many of the new instances were provisioned with the usage ping disabled
      by default.
    - Improvement - Continue high level efforts on [simplifying groups/projects](https://about.gitlab.com/company/team/structure/working-groups/simplify-groups-and-projects/)
      and [making issues more extensible](https://gitlab.com/groups/gitlab-org/-/epics/3354)
      while also making incremental progress on [iterations](https://gitlab.com/groups/gitlab-org/-/epics/2422)
  instrumentation:
    level: 2
    reasons:
    - Implementation & Dashboard - Lacking a [standardized data model](https://gitlab.com/gitlab-data/analytics/-/issues/5682)
      to generate a [blended dashboard that accurately distinguishes between Paid
      and Free users](https://gitlab.com/gitlab-data/analytics/-/issues/4291#note_384129651).
  sisense_data:
    chart: 9422714
    dashboard: 722212
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/722212/Plan:-Project-Management-GMAU
- name: Plan:Portfolio Management - Paid GMAU - MAU creating Epics each Month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users creating Epics each month.
  target: 1,500 Unique Users for Self Managed and 1,200 Unique Users for SaaS
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - Substantial growth in the past few months driven by investments in
      Epics, and moving Epics down to Premium (note - that move did not canabilize
      Ultimate adoption)
    - Improvement - Swimlanes will be a significant driver of MAU growth for Portfolio
      Management in the nearterm (n2m) - giving visibility to the feature on boards
      and adding additional value to better visualize your in-flight issues through
      swimlanes. Epic Boards should continue the trend into the end of FY21 by enabling
      Portfolio-level planning.
  instrumentation:
    level: 3
    reasons:
    - Dashboarding - we've instrumented core metrics but are looking to expand the
      MAU definition to include all epic usage, not just creation, for a more accurate
      GMAU metric | [Issue](https://gitlab.com/gitlab-data/analytics/-/issues/5280)
  sisense_data:
    chart: 9245394
    dashboard: 602355
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5280
- name: Plan:Certify - Paid GMAU - MAU Interacting with Requirements
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users interacting (create, view, reference) each
    month
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Dashboarding not implemented yet so health is a proxy of our Certify's "other"
      metric and adhnoc anlaysis. [Tracking issue](https://gitlab.com/gitlab-data/analytics/-/issues/5270)
    - Improvement - UX improvements for [Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/3708)
      based on completed research study, Requirement [Import functionality](https://gitlab.com/gitlab-org/gitlab/-/issues/233535),
      Upcoming [Quality Management MVC](https://gitlab.com/groups/gitlab-org/-/epics/3852)
  instrumentation:
    level: 2
    reasons:
    - Dashboarding- Events are instrumented but we still need dashboards. Tracking
      issue [here](https://gitlab.com/gitlab-data/analytics/-/issues/5270)
- name: Plan:Certify - Other - Number of Requirements Created
  base_path: "/handbook/product/performance-indicators/"
  definition: A monthly count of the number of requirements created.
  target: 2000 by end of August 2020
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - still small as a proportion of Stage usage but growing consistently
      since 12.10 launch
  instrumentation:
    level: 2
    reasons:
    - Dashboarding -  accurate instrumentation on Self Managed, still needs verification
      for SaaS | [issue](https://gitlab.com/gitlab-data/analytics/-/issues/5270)
    - Dashboarding - update metrics collection to [track interactions](https://gitlab.com/gitlab-org/telemetry/-/issues/370)
  sisense_data:
    chart: 9109758
    dashboard: 698633
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5270
- name: Create - SMAU - MAU Conducting Git Write Operations
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed any Git
    write operation within the last 28 days.
  target: Our FY21 Q3 Target is to exceed 1m unique users performing Git write operations.
    As instances upgrade to GitLab 13.3 with improved instrumentation, we intend to
    switch to growth rate target aligned with user growth rates.
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - Git write operations are performed by a large number of users. On
      GitLab.com, approximately 10% of users perform a Git write operation every month.
      For licensed self-hosted instances, this number should be much higher.
  instrumentation:
    level: 2
    reasons:
    - Enabled on GitLab.com for testing, and is scheduled for self-hosted in GitLab
      13.3. https://gitlab.com/gitlab-org/gitlab/-/issues/230481
    - Dashboard under construction https://gitlab.com/gitlab-data/analytics/-/issues/5629
    - AMAU is being tracked per repository type, and which means that we are under
      counting slightly by using project repositories at the moment. https://gitlab.com/gitlab-org/gitlab/-/issues/234027
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230481
  - https://gitlab.com/gitlab-org/gitlab/-/issues/234027
  - https://gitlab.com/gitlab-data/analytics/-/issues/5629
- name: Create:Gitaly - GMAU - MAU Conducting Git Write Operations
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed any Git
    write operation within the last 28 days.
  target: Our FY21 Q3 Target is to exceed 1m unique users performing Git write operations.
    As instances upgrade to GitLab 13.3 with improved instrumentation, we intend to
    switch to growth rate target aligned with user growth rates.
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Improvement - Gitaly continues to invest in Gitaly Cluster which is a driver
      of growth. https://gitlab.com/groups/gitlab-org/-/epics/1489
    - Improvement - Gitaly continues to invest in enormous repository support, to
      enable growth into new markets where centralized version control systems are
      dominant https://gitlab.com/groups/gitlab-org/-/epics/773
  instrumentation:
    level: 2
    reasons:
    - Enabled on GitLab.com for testing, and is scheduled for self-hosted in GitLab
      13.3. https://gitlab.com/gitlab-org/gitlab/-/issues/230481
    - Dashboard under construction https://gitlab.com/gitlab-data/analytics/-/issues/5629
    - AMAU is being tracked per repository type, and which means that we are under
      counting slightly by using project repositories at the moment. https://gitlab.com/gitlab-org/gitlab/-/issues/234027
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230481
  - https://gitlab.com/gitlab-org/gitlab/-/issues/234027
  - https://gitlab.com/gitlab-data/analytics/-/issues/5629
- name: Create:Gitaly - Other - Availability
  base_path: "/handbook/product/performance-indicators/"
  definition: Percentage of time during which Gitaly is fully operational and providing
    service to users within SLO parameters. Availability is measured according by
    Apdex (latency) and error rate. Gitaly is considered available when at least 50%
    of users are experiencing satisfactory latency, _and_ 50% of requests are completing
    successfully. If either of these conditions is not met, the service is experiencing
    an outage.
  target: 99.95%, consistent with GitLab.com availability service level objectives
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - Gitaly continues to be a reliable service for customers on GitLab.com
      and self hosted
    - Improvement - Gitaly Cluster provides opportunity for higher availability, particularly
      to defend against resource exhaustion availability problems, and work is in
      early stages to develop a roadmap for a cost effective deployment on GitLab.com
      https://gitlab.com/groups/gitlab-org/-/epics/3372
  instrumentation:
    level: 2
    reasons:
    - Rolling 7 day metrics available in <a href="https://dashboards.gitlab.com/d/gitaly-main/gitaly-overview">Grafana</a>
      for GitLab.com.
    - Self-hosted availability metrics are planned https://gitlab.com/gitlab-org/gitlab/-/issues/216683
  urls:
  - https://dashboards.gitlab.com/d/gitaly-main/gitaly-overview
  - https://gitlab.com/gitlab-org/gitlab/-/issues/216683
- name: Create:Source Code - GMAU - MAU Conducting Git Write Operations to project
    repository
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed a Git write
    operation to a project repository within the last 28 days
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - No target defined.
    - Improvement - work towards providing users more insights while navigating their
      code with [Code Intelligence](https://gitlab.com/groups/gitlab-org/-/epics/1576)
    - Improvement - work towards providing more control over [managing repository
      size](https://gitlab.com/groups/gitlab-org/-/epics/3793)
  instrumentation:
    level: 2
    reasons:
    - Enabled on GitLab.com for testing.
    - Scheduled for self-hosted in GitLab 13.3.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230481
  - https://gitlab.com/gitlab-data/analytics/-/issues/5629
- name: Create:Source Code - Other - Percentage of users per instance using Merge
    Requests
  base_path: "/handbook/product/performance-indicators/"
  definition: Proportion of users per each instance (self-managed) or userspace (GitLab.com)
    making use of Merge Requests. This is defined as users who perform one or more
    of the following actions in a merge request - create, merge, or close a Merge
    Request.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 2
    reasons:
    - No target defined.
    - Improvement - work towards enhancing the [UX of Merge Requests](https://gitlab.com/groups/gitlab-org/-/epics/3607)
    - Improvement - work towards better coordination of [cross-stage changes to the
      Merge Requests UX](https://gitlab.com/groups/gitlab-org/-/epics/3776)
    - Improvement - work towards [making suggested changes lovable](https://gitlab.com/groups/gitlab-org/-/epics/832)
  instrumentation:
    level: 2
    reasons:
    - Sisense chart is being constructed.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4633
- name: Create:Knowledge - GMAU - MAU that viewed or uploaded designs
  base_path: "/handbook/product/performance-indicators/"
  definition: Monthly unique users that have viewed or uploaded designs.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - No target defined yet
    - Recent improvements - Initially we want to drive usage and feature adoption.
      We moved the feature to core and moved designs up in the UI so they will be
      seen by users. The chart above just shows Design Views on .com
    - Improvement - We plan to show designs in more places in the app to increase
      adoption
  instrumentation:
    level: 2
    reasons:
    - Have .com data for design views
    - Instrumenting usage ping and updates/creates for 13.4
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/234041
  - https://gitlab.com/gitlab-data/analytics/-/issues/4761
  sisense_data:
    chart: 9420379
    dashboard: 600878
    embed: v2
- name: Create:Knowledge - Other - Design Management Git Write Operations
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed a Git write
    operation to a design repository within the last 28 days
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined.
  instrumentation:
    level: 2
    reasons:
    - Enabled on GitLab.com for testing.
    - Scheduled for self-hosted in GitLab 13.3.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230481
- name: Create:Knowledge - Other - Wiki Git Write Operations
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed a Git write
    operation to a wiki repository within the last 28 days
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined.
  instrumentation:
    level: 2
    reasons:
    - Enabled on GitLab.com for testing.
    - Scheduled for self-hosted in GitLab 13.3.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230481
- name: Create:Static Site Editor - GMAU - MAU that committed via the SSE
  base_path: "/handbook/product/performance-indicators/"
  definition: Monthly unique users that have made at least one commit via the static
    site editor.
  target: 110 MAU by September 30, 2020
  org: Dev Section
  public: true
  telemetry_type: 
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Our goal is to drive early adoption, with a focus on internal GitLab team members
      editing the Handbook
    - Static Site Editor to be [available for all Handbook pages](https://gitlab.com/gitlab-org/gitlab/-/issues/221254)
      in 13.3
  instrumentation:
    level: 2
    reasons:
    - Usage Ping data is missing so the data doesn't reflect self-hosted users
    - Working on instrumenting for [Usage Ping in 13.4](https://gitlab.com/gitlab-org/gitlab/-/issues/233994)
  sisense_data:
    chart: 9112733
    dashboard: 684213
    embed: v2
- name: Create:Editor - GMAU - MAU Conducting Git Write Operations from web editors
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of the number of unique users who performed a Git write
    operation to any respository using the Web IDE, Web Editor or Snippet within a
    stage in the last 28 days
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - The metric has not been instrumented yet.
  instrumentation:
    level: 1
    reasons:
    - Instrumentation plan has been finalized and due to capacity constraints is scheduled
      to be completed in %13.4 and %13.5.
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4014
- name: Create:Editor - Other - Number of Commits per Month from the Web IDE
  base_path: "/handbook/product/performance-indicators/"
  definition: The total number of commits made via the Web IDE.
  target: Unknown until we reach maturity level 2
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined.
  instrumentation:
    level: 2
    reasons:
    - Instrumentation not yet available for SaaS.
  sisense_data:
    chart: 9438336
    dashboard: 723366
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4398
- name: Create:Editor - Other - Number of new Snippets per Month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of new Snippets created each month.
  target: Unknown until we reach maturity level 2
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined.
  instrumentation:
    level: 3
    reasons:
    - Instrumentation for number of snippets is complete for self-managed and SaaS.
  sisense_data:
    chart: 8762341
    dashboard: 656754
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4400
- name: Create:Ecosystem - GMAU - MAU of any integration
  base_path: "/handbook/product/performance-indicators/"
  definition: Monthly Unique Users of any GitLab integration.
  target: TBD
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - No target defined
    - Improvement - Adding Group and Instance integration will allow larger sets of
      projects to be integrated easily
    - Improvement - Improvements and new functionality in the Jira integration should
      increase adoption, expanding on the success of our most popular integration
  instrumentation:
    level: 1
    reasons:
    - Awaiting implementation of data in Usage Ping
    - Awaiting implementation of Sisense dashboard (blocked by Usage Ping implementation)
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/225678
  - https://gitlab.com/gitlab-data/analytics/-/issues/5850
- name: Create:Ecosystem - Other - Total active project integrations
  base_path: "/handbook/product/performance-indicators/"
  definition: The total number of active project service integrations across all usage
    ping data (which includes GitLab.com)
  target: Maintain flat adoption rate relative to overall GitLab growth
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Observation - Growth tracks fairly evenly against overall usage growth (total
      number of projects created), demonstrating flat relative adoption rate of integrations
    - Observation - GitLab.com users integrate at ~1/3 the rate that self-hosted users
      do
  instrumentation:
    level: 3
    reasons:
    - Good overall tracking of project integrations, detailed breakdown available
      per-integration
  sisense_data:
    chart: 8583744
    dashboard: 577690
    embed: v2
  urls: 
- name: Create:Ecosystem - Other - Integrations managed at Group or Instance level
  base_path: "/handbook/product/performance-indicators/"
  definition: The total number of active project integrations that are being defined
    by a Group or Instance integration
  target: Not defined
  org: Dev Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No data so far
  instrumentation:
    level: 0
    reasons:
    - Usage ping recently implemented, no Dashboard yet
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/204802#note_392267011
  - https://gitlab.com/gitlab-data/analytics/-/issues/5849
