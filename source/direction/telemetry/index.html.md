---
layout: markdown_page
title: Product Direction - Telemetry
description: "Telemetry manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/telemetry/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/index.html)                                                    | An overview of our collection framework                   |
| [Event Dictionary](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)                                        | A SSoT for all collected metrics and events               |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/telemetry/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Privacy Policy](https://about.gitlab.com/privacy/)                                                                               | How we handle our user's privacy and data                 |
| [Product Performance Indicators Workflow](https://about.gitlab.com/handbook/product/performance-indicators#pi-workflow)           | The workflow for putting product PIs and XMAUs in place   |
| [Data for Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/)  | How to create your own dashboard                |
| [Data Warehouse](https://about.gitlab.com/handbook/business-ops/data-team/platform/#data-warehouse)                               | An outline of where our product usage data is stored      |
| [dbt Guide](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/)                                         | How we transform raw data into a data structure that's ready for analysis | 

## Overview

Telemetry manages a variety of technologies that are important for GitLab's understanding of how our users use our products. These technologies include but are not limited to: Snowplow Analytics and an in-house tool called Usage Ping which is hosted on `version.gitlab.com` and includes a separate service called[ Version Check](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#version-check-core-only).

If you'd like to discuss this vision directly with the Product Manager for Telemetry, feel free to reach out to Hila Qu via [e-mail](mailto:hilaqu@gitlab.com).

The primary purpose of telemetry is to help us build a better Gitlab. Data about how Gitlab is used is collected to better understand what parts of Gitlab needs improvement and what features to build next. Telemetry also helps our team better understand the reasons why people use Gitlab and with this knowledge we are able to make better product decisions.

The overall vision for the Telemetry group is to ensure that we have a robust, consistent and modern telemetry data framework in place to best serve our internal Product, Finance, Sales, and Customer Success teams. The group also ensures that GitLab has the best visualization and analysis tools in place that allows the best possible insights into the data provided through the various collection tools we utilize.

| Category | Description |
| ------ |  ------ |
| [🧺 Collection](/direction/telemetry/collection) | The structure(s) and platform(s) of how we collect Telemetry data |
| [🔍 Analysis](/direction/telemetry/analysis) | Manages GitLab's internal needs for an analysis tool that serves the Product department |

## Roadmap FY21-Q3

### Product Performance Indicators

Lead the [Product Org OKR's for Q3](https://gitlab.com/gitlab-com/Product/-/issues/1320). For more information, see [Product Performance Indicators](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit).

- KR1 (EVP, Product): [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- KR2 (EVP, Product): [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343)

**Deliverables:**

- [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)
- [Add All PIs to Product PI Pages](https://gitlab.com/groups/gitlab-com/-/epics/906)

### Deploy Telemetry for Sales and Customer Success

Support the [FY21-Q3 Deploying Telemetry for CRO Org](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#). For more information, see [CRO Telemetry: Status, Gaps and the Road Forward](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#).

**Deliverables:**

- [Customer Adoption Journey](https://gitlab.com/groups/gitlab-org/-/epics/3572)

### Rollout Privacy Policy for Product Usage Data

Support the [Privacy Policy Rollout](https://gitlab.com/groups/gitlab-com/-/epics/907)

**Deliverables:**

- [Privacy Policy for Product Usage Data](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8780)

### Collection Framework

**Current State:**

1. Usage Ping (SaaS + SM). **Not useful for multi-tenant SaaS as it’s missing Group (namespace) and Plan.**
1. Snowplow (SaaS). **Not identifiable to a Group or User and cannot be used for MAU. Eventually available on self-managed.**
1. Database Imports (SaaS). **Not available on self-managed.**

![](collection_framework_fy21_q3.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

**Roadmap:**

1. Q3: Usage Ping Redis (SaaS + SM).
1. Q3: Plan-level reporting of Usage Ping (SaaS).
1. Q4: User-level reporting on Snowplow (SaaS + SM).
1. Q4: Group-level reporting on Snowplow (SaaS + SM).
1. Q4: Snowplow (SM).
1. FY22-Q1: Group-level reporting on Usage Ping (SaaS).

![](collection_framework_fy21_q4.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

### Scale and Maintain Usage Ping

**Current State:**

- Generating a Usage Ping on GitLab.com now takes **over 24 hours**. This is a **120% increase** from the [11 hours](https://gitlab.com/gitlab-org/gitlab/-/issues/228571#note_388866586) it previously took when we initially fixed and re-enabled Usage Ping five months ago. This increase in generation time is due to the amount of new Usage Ping counters we’ve added along with the rapidly growing size of GitLab.com’s database tables. The payload size of Usage Ping has [increased from 376 metrics to 544+ metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/228571#note_388930798). 
- Usage Ping currently does not have a retry or caching mechanism. After spending the time generating a Usage Ping, if a network error occurs when passing sending data to us, the Usage Ping is lost and we will need to wait for the next scheduled cron job 7 days later.

**Roadmap:**

- Compute Usage Ping counters in parallel instead of serially.
- Add retry logic and caching for Usage Pings.

### Instrument Tracking

**Current State:**

- To support [Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/) work, we need to guide and review each product group’s tracking code. In some cases, we will need to directly do the implementation work.
- Sales and CS are starting to get Product Usage Data into Salesforce and Gainsight. They are currently looking at defining a Customer Adoption Journey which will require instrumentation of events throughout the GitLab application.

**Roadmap:**

- Support [Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/) work.
- Instrument events for Sales and CS [Customer Adoption Journey](https://gitlab.com/groups/gitlab-org/-/epics/3572).
- De-duplicate for GMAU / SMAU metrics.
- Find intersection between multiple Usage Ping metrics.
