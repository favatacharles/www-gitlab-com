---
layout: markdown_page
title: "Category Direction - Advanced Deployments"
---
 
- TOC
{:toc}

## Advanced Deployments 

Advanced deployment techniques help teams control the deployment of new software
versions as well as the rollout of new features to users. This includes the ability to release
features to a limited audience, monitor the performance and behavior, and make a conscious decision 
whether to continue the rollout or if needed, to rollback.

- [Issue List](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAdvanced%20Deployments)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=Category%3AAdvanced%20Deployments)

### Blue/green deploys

Create two (nearly) identical environments, arbitrarily called blue and green. One isn't better than the other, just separate. With all traffic going to green, load new code onto blue, get it ready, and then switch the router to suddenly, as quickly as possible, send all new traffic to blue.

![Blue_Green](/images/direction/cicd/blue_green.jpg)

### Recreate in place

This is a very simple deployment where all of the old pods are killed and replaced all at once with the new ones.

### Canary

In a Canary Release, you start with all your machines/dockers/pods at 100% of the current deployment. When you are ready to try out your new deployment on a subset of your users, you send those user's traffic to the new deployment, while the others are still on the current one. The terms canary release and incremental release are very similar and often used interchangeably. 

![Canary](/images/direction/cicd/canary.jpg)

## What's Next & Why

In some cases, you may want to "canary" a new set of changes by sending a small number of requests to a different service than the production service. The canary annotation enables the Ingress spec to act as an alternative service for requests to route depending on the rules applied. We are working on extending support for optional NGNIX Ingress annotations via epic [gitlab#3142](https://gitlab.com/groups/gitlab-org/-/epics/3142). 
We are starting with [gitlab#215501](https://gitlab.com/gitlab-org/gitlab/-/issues/215501) which lets you set the canary's weight via an API for projects using Kubernetes.

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- [Implement Blue/Green Switch on Deploy Boards](https://gitlab.com/gitlab-org/gitlab/-/issues/218140)
- [Keep Environment in standby for a limited time after new deployment is rolled out](https://gitlab.com/gitlab-org/gitlab/-/issues/35409)
- [Update NGNIX Ingress Parameters annotations to Helm](https://gitlab.com/groups/gitlab-org/-/epics/3142)
- [Add a "Resize" button to the deploy boards](https://gitlab.com/gitlab-org/gitlab/-/issues/199437)
- [Increase visibility of incremental rollouts in deploy boards](https://gitlab.com/gitlab-org/gitlab/-/issues/5417)
- [Move features to core: "Incremental Rollout"](https://gitlab.com/gitlab-org/gitlab/-/issues/212316)
- [Move features to core: "Canary Deployments"](https://gitlab.com/gitlab-org/gitlab/-/issues/212319#note_383945661)
- [Blue/Green Deployments for AWS ECS](https://gitlab.com/gitlab-org/gitlab/-/issues/226994)
- [Canary Deployments for AWS ECS](https://gitlab.com/gitlab-org/gitlab/-/issues/226998)

## Top Customer Success/Sales Issue(s)
 
Our top customer success issue is [gitlab#8463](https://gitlab.com/gitlab-org/gitlab/-/issues/8463). 
This bug shows duplicate instances on the deploy board. In case an application has multiple deployments, the deploy board will show an inappropriate number of pods. 

## Top Customer Issue(s)
 
Our top customer issue is [gitlab#212320](https://gitlab.com/gitlab-org/gitlab/-/issues/212320). It aims to bring our Deploy Boards into GitLab's open-source Core product, allowing everyone to benefit from this functionality.

## Top Vision Item(s)

Our top vision item is [gitlab#226994](https://gitlab.com/gitlab-org/gitlab/-/issues/226994) which introduces advanced deployments for non-Kubernetes users.
